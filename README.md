# WoSIS Graphql API Masterclass


# Introduction

WoSIS stands for 'World Soil Information Service', a large PostgreSQL database developed and maintained by ISRIC, WDC-Soils. It provides a growing range of quality-assessed and standardised soil profile data for the world. For this, it draws on voluntary contributions of data holders/providers worldwide.

The source data come from different types of surveys ranging from systematic soil surveys (i.e., full profile descriptions) to soil fertility surveys (i.e., mainly top 20 to 30 cm). Further, depending on the nature of the original surveys the range of soil properties can vary greatly (see https://essd.copernicus.org/articles/12/299/2020/).

The quality-assessed and standardised data are made available freely to the international community through several webservices, this in compliance with the conditions (licences) specified by the various data providers. This means that we can only serve data with a so-called 'free' licence to the international community (https://data.isric.org/geonetwork/srv/eng/catalog.search#/search?any=wosis_latest). A larger complement of geo-referenced data with a more restrictive licence can only be used by ISRIC itself for producing SoilGrids maps and similar products (i.e. output as a result of advanced data processing). Again, the latter map layers are made freely available to the international community (https://data.isric.org/geonetwork/srv/eng/catalog.search#/search?resultType=details&sortBy=relevance&any=soilgrids250m%202.0&fast=index&_content_type=json&from=1&to=20). 

During this master class, you will first learn what GraphQL and API (application programming interface) are. Next, using guided steps, we will explore the basics of WoSIS and GrapQL via a grahical interface. Building upon this, we will move into a coding platform and create a simple project that uses soil data. From that point onwards we will slowly increase complexity and use WoSIS data in different scenarios. 

The workshop requires no previous knowledge of WoSIS or GraphQL. However, it is advisable to have basic coding knowledge on the Python or R language. 

The workshop is hosted on GitHub and public access (MIT licence). 

